## IOC feeds 
1) **[ThreatCrowd](http://www.threatcrowd.org)**

_ Search Engine for Threats || ThreatCrowd is now powered by **AlienVault®**

2) **[ISC Suspicious domains](http://isc.sans.edu/suspicious_domains.html)**

_ Internet Storm Center has created an aggregated list of suspicious domains, each of which is weighted by tracking and by malware lists from several sources.

3) **[ThreatMiner](http://www.threatminer.org)**

_ ThreatMiner is a site that enables the user to conduct data mining for indicators of compromise. The search mechanism is crowd-supported, and can be use to supplement search operations across domains, IPs, SSL, and a wide variety of other indicator types.

4) **[Critical Stack Intel](https://intel.criticalstack.com)**

_ Threat Intelligence 100% Free for [Bro intelligence framework](https://www.bro.org) - aggregated, parsed and delivered by **_Critical Stack,Inc_**

5) **[Barracuda Central](http://barracudacentral.org)** -- threat intelligence of **_Barracuda_**

+ [Barracuda Reputation Block List](http://www.barracudacentral.org/account/register) (need Request access!) || [BRBL How-to](http://www.barracudacentral.org/rbl/how-to-use) 
+ [Spam Data](http://www.barracudacentral.org/data/spam) ||| [Web Data](http://www.barracudacentral.org/data/web) ||| [IP/Domains Lookup](http://www.barracudacentral.org/lookups)  
