<p align="center"><img src="https://i.imgur.com/lcWjcMf.png"></p>

## 101
### >[your Practical Guide to Threat Hunting](https://sqrrl.com/media/Your-Practical-Guide-to-Threat-Hunting.pdf) 

### >[Definite Guide to Cyber Threat Intelligence](https://cryptome.org/2015/09/cti-guide.pdf)

### >[Standards related to Threat Intelligence](https://www.threat-intelligence.eu/standards)

### >[@RiskIQ -- Threat Hunting Workshop: Datasets](https://www.youtube.com/playlist?list=PLgLzPE5LJevYLWdBzPcxZ0ozFjpmHBPN9)

## Splunk
### ⭐ [Hunting with Splunk: The basics](https://www.splunk.com/blog/2017/07/06/hunting-with-splunk-the-basics.html)

### [Threat Hunting with Splunk v2017](https://www.slideshare.net/Splunk/threat-hunting-with-splunk-76027177)

### [Splunk - ADVANCED THREAT DETECTION AND RESPONSE](https://www.splunk.com/pdfs/technical-briefs/advanced-threat-detection-and-response-tech-brief.pdf) 

### [Advanced Incident Detection & Threat Hunting using Sysmon and Splunk](https://www.first.org/resources/papers/conf2017/Advanced-Incident-Detection-and-Threat-Hunting-using-Sysmon-and-Splunk.pdf)

### [Hunting Botnets: Suricata & Splunk Advanced Security Analytics](https://suricon.net/wp-content/uploads/2017/12/SuriCon17-Tellez_Hunting_Botnets.pdf)

***

## Cyb3rWard0g 
### [ThreatHunter-Playbook](https://github.com/Cyb3rWard0g/ThreatHunter-Playbook) - to aid the development of techniques and hypothesis for hunting campaigns.
### [OSSEM](https://github.com/Cyb3rWard0g/OSSEM) - Open Source Security Events Metadata (OSSEM). The need for a global schema!
### [HELK](https://github.com/Cyb3rWard0g/HELK) - Hunting ELK (Elasticsearch, Logstash, Kibana) with advanced analytic capabilities.

*** 
## Microsoft Advanced Threat 

### [Microsoft/WindowsDefenderATP-Hunting-Queries](https://github.com/Microsoft/WindowsDefenderATP-Hunting-Queries) 
- This repo contains sample queries for Advanced hunting on Windows Defender Advanced Threat Protection. With these sample queries, you can start to experience Advanced hunting, including the types of data that it covers and the query language it supports. You can also explore a variety of attack techniques and how they may be surfaced through Advanced hunting.


### [Evading Microsoft ATA Day1_Day5](http://www.labofapenetrationtester.com/2017/08/week-of-evading-microsoft-ata-day1.html)
### [Microsoft -- Best practices for securing Advanced Threat Analytics](https://cloudblogs.microsoft.com/enterprisemobility/2016/06/10/best-practices-for-securing-advanced-threat-analytics) 
### [Defending Microsoft env at scale](https://drive.google.com/file/d/1QXjmlPRvfiRBnqzNpsTQo5bn0xKQoNc4/view) 
### [Microsoft Advanced Threat Analytics (ATA) Documentation](https://docs.microsoft.com/en-us/advanced-threat-analytics)
### [Tales from the SOC: Real-world Attacks Seen Through Defender ATP](https://www.slideshare.net/MSbluehat/bluehat-v17-tales-from-the-soc-realworld-attacks-seen-through-defender-atp)

***

## MITRE ATT&CK
### [Adversarial Tactics, Techniques & Common Knowledge a.k.a ATT&CK](https://attack.mitre.org/wiki/Main_Page) 
- MITRE’ ATT&CK™ is a curated knowledge base and model for cyber adversary behavior, reflecting the various phases of an adversary’s lifecycle and the platforms they are known to target. ATT&CK is useful for understanding security risk against known adversary behavior, for planning security improvements, and verifying defenses work as expected.

### [Tableau-ATTCK](https://github.com/Cyb3rPanda/Tableau-ATTCK) - Understanding ATT&CK Matrix for Enterprise 

### [ATT&CK Navigator v2.1](https://mitre.github.io/attack-navigator/enterprise) 
- The principal feature of the Navigator is the ability for users to define layers - custom views of the ATT&CK knowledge base - e.g. showing just those techniques for a particular platform or highlighting techniques a specific adversary has been known to use. Layers can be created interactively within the Navigator or generated programmatically and then visualized via the Navigator.

### [DeTTACT](https://github.com/rabobank-cdc/DeTTACT) -- Detect Tactics, Techniques & Combat Threats, formerly Blue-ATT&CK

### ⭐ [ATT&CK-Tools](https://github.com/nshalabi/ATTACK-Tools) - Utilities for MITRE ATT&CK (View & Data Model)

### [BlueTeamToolkit/sentinel-attack](https://github.com/BlueTeamToolkit/sentinel-attack) - Sentinel ATT&CK aims to simplify the rapid deployment of a threat hunting capability that leverages Sysmon and MITRE ATT&CK on Azure Sentinel.

# Changelog v2018
### MARCH

### [Enhanced PowerShell logging & Shipping Logs to an ELK Stack for Threat Hunting](https://cyberwardog.blogspot.com/2017/06/enabling-enhanced-ps-logging-shipping.html) 





### APRIL 

### [MacOSX backdoor linked to OceanLotus](https://blog.trendmicro.com/trendlabs-security-intelligence/new-macos-backdoor-linked-to-oceanlotus-found) with @[TrendLabs Security Intelligence](https://blog.trendmicro.com/trendlabs-security-intelligence)

### [Detecting the Elusive: AD Threat Hunting](https://adsecurity.org/wp-content/uploads/2017/04/2017-BSidesCharm-DetectingtheElusive-ActiveDirectoryThreatHunting-Final.pdf)

### [Discrupting the Mirai botnets](https://www.slideshare.net/MSbluehat/bluehat-v17-disrupting-the-mirai-botnet)



### [Detecting compromise on Windows endpoints with OSquery](https://www.slideshare.net/MSbluehat/bluehat-v17-detecting-compromise-on-windows-endpoints-with-osquery-84024735)

### [Using TLS certificates to track activity groups](https://www.slideshare.net/MSbluehat/bluehat-v17-using-tls-certificates-to-track-activity-groups)

### MAY

### [Advance hunting with RSA Netwitness](https://cyber-ir.com/2017/09/23/advance-hunting-with-rsa-netwitness)

### ⭐ [Sqrrl/ ThreatHunting blog](https://sqrrl.com/blog)


### JUNE 

### ⭐ [CyberWar Map --part of Cyber Vault Project](https://embed.kumu.io/0b023bf1a971ba32510e86e8f1a38c38#apt-index)
- The CyberWar Map is a visual guide to some of the most prominent players and events in state-to-state cyberconflict created as a part of the **National Security Archive's Cyber Vault Project**. This resource focuses on state-sponsored hacking and cyber-attacks.
